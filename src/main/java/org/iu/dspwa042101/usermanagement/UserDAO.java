package org.iu.dspwa042101.usermanagement;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/dspwa042101";
    private static final String DB_USER = "torbenwetter";

    private static boolean driverInitialized = false;

    public static void addUser(User user) {
        try {
            Connection db = getConnection();

            String sql = "INSERT INTO users (first_name, last_name) VALUES (?, ?)";
            PreparedStatement stmt = db.prepareStatement(sql);
            stmt.setString(1, user.getFirstName());
            stmt.setString(2, user.getLastName());
            stmt.execute();

            db.close();
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static List<User> getUsers() {
        try {
            Connection db = getConnection();

            ResultSet rs = db.createStatement().executeQuery("SELECT * FROM users");
            List<User> users = new ArrayList<>();
            while (rs.next()) {
                User user = new User(rs.getString("first_name"), rs.getString("last_name"));
                users.add(user);
            }

            db.close();

            return users;
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static Connection getConnection() throws ClassNotFoundException, SQLException {
        if (!driverInitialized) {
            Class.forName("org.postgresql.Driver");
            driverInitialized = true;
        }

        Connection db = DriverManager.getConnection(DB_URL, DB_USER, null);

        String sql = "CREATE TABLE IF NOT EXISTS users (id SERIAL PRIMARY KEY, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL)";
        db.createStatement().execute(sql);

        return db;
    }
}
