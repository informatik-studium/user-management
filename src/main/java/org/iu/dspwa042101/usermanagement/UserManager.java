package org.iu.dspwa042101.usermanagement;

import java.util.List;

public class UserManager {

    private static UserManager instance;

    private UserManager() {
    }

    public static UserManager getInstance() {
        if (instance == null) {
            instance = new UserManager();
        }
        return instance;
    }

    public List<User> getUsers() {
        return UserDAO.getUsers();
    }

    public void addUser(User user) {
        UserDAO.addUser(user);
    }
}
