<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Neue:r Nutzer:in</title>
</head>
<body>
<h1>Registrierung</h1>
<form action="/User_Management_war_exploded/register" method="post">
    <p>
        <label for="firstName">Vorname:</label>
        <input type="text" name="firstName" id="firstName"/><br/>
    </p>
    <p>
        <label for="lastName">Nachname:</label>
        <input type="text" name="lastName" id="lastName"/><br/>
    </p>
    <button type="submit">Registrieren</button>
</form>
<p><a href="/User_Management_war_exploded">Zurück zur Startseite</a></p>
</body>
</html>
